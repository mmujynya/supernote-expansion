import os
import shutil
from msal import PublicClientApplication, ConfidentialClientApplication
from msgraph import GraphServiceClient
from azure.identity.aio import ClientSecretCredential
import asyncio
import requests
import json
import datetime
import mimetypes
from PIL import Image

import supernotelib as sn
from supernotelib.converter import ImageConverter, PdfConverter #, SvgConverter, TextConverter
from supernotelib.converter import VisibilityOverlay
import ctypes
import fitz  # PyMuPDF

# --------------------------------------------------------------------------------------
# Max Mujynya, March 20 2024
#---------------------------------------------------------------------------------------

# Some preps to hide the terminal poping up, for tasks 
# launched by the Windows task scheduler
# Get the handle to the console window
console_window = ctypes.windll.kernel32.GetConsoleWindow()
# Minimize the window
ctypes.windll.user32.ShowWindow(console_window, 6)

# Global variable initialization
ACCESS_TOKEN = None # The Global variable used to store the current session Access Token

# Get the current working directory
current_working_directory = os.getcwd()
SN_LOCAL_FOLDER = os.path.join(current_working_directory, 'temp') # The user local folder where interim info is stored
TOC_PDF_FOLDER = os.path.join(current_working_directory, 'PDF Notes') # The destination folder

# The text filename where the delta link is stored
# Warning! Deleting this will cause the script to process all files in the watched folder
SN_LOCAL_DELTA_FILENAME = 'od_delta.txt' 

# The name of the remote folder to watch
# All MS Graph queries are made using a Graph DriveID identifier,
# this variable is used by a recursive function to locate a folder name
# matching this name. Found DriveID is stored in SN_LOCAL_WATCHING_FOLDER_FN
SN_LOCAL_WATCHING_FOLDER = 'Note' 
SN_LOCAL_WATCHING_FOLDER_FN = 'folder_id.txt' # The text filename where the watched folder ID is saved

# ------------------------------------------------------------------------
# Microsoft Azure/Graph credentials, stored as a user Environment Variable
# ------------------------------------------------------------------------

# The client ID stored provided by Azure when registering this app
CLIENT_ID = os.getenv("SN_CLIENT_ID", "") 

# On MS Azure, this is the ObjectID of the user owning the OneDrive folder
USER_ID = os.getenv("SN_USER_ID", "") 

TENANT_ID = os.getenv("SN_TENANT_ID", "") # The Tenant ID stored in the ENV
CLIENT_SECRET = os.getenv("SN_CLIENT_SK", "") # The Secret key stored in the ENV 
AUTHORITY_URL = f'https://login.microsoftonline.com/{TENANT_ID}'
RESOURCE_URL = 'https://graph.microsoft.com'
API_VERSION = 'v1.0'

# Application permissions scopes
# TODO: Failed to have the app authenticate using delegated authentication,
# likely because app is running locally (not on a server), so reverted to application permissions
# with secret token
SCOPES = ['https://graph.microsoft.com/.default']
BASE_GRAPH_URL = f'{RESOURCE_URL}/{API_VERSION}'

# TODO: Due to extreme lazyness, switched to MS Graph SDK in the middle of
# this project. Leaving for now till trying to improve the script
credential = ClientSecretCredential(TENANT_ID,
                                    CLIENT_ID,
                                    CLIENT_SECRET)
graph_client = GraphServiceClient(credential, SCOPES)

# Supernote variables

# Opening SN png files in Photoshop, I see images are 1404x1872 pixels with 72 pixels/inch
MAX_HORIZONTAL_PIXELS = 1404 # SN horizontal definition
MAX_VERTICAL_PIXELS = 1872 # SN vertical definition
TITLE_VERTICAL_SPACE = 40 # Vertical space between titles in the Table of Contents
TITLE_HORIZONTAL_SPACE = 55 # Horizontal space between titles in the Table of Contents, used for identation
TITLE_STYLES = {"1000254":1, "1157254":2, "1201000":3,"1000000":4} # 'Rank' of SN styles

# Stores details on notebooks that have been changed since the last MS Graph polling
changed_items = []

# Visibility parameters for the png conversion
bg_visibility = VisibilityOverlay.DEFAULT
vo = sn.converter.build_visibility_overlay(background=bg_visibility)


#------------------------  FUNCTIONS  SECTION ------------------------------------------

# Access is given through application permissions on Azure
# Retrieve Access_Token and store it in ACCESS_TOKEN
def set_access_token():
    global ACCESS_TOKEN
    try:
        print('\n')
        print('>> Microsoft authentication ... ', end="")
        app = ConfidentialClientApplication(CLIENT_ID,authority=f"https://login.microsoftonline.com/{TENANT_ID}", client_credential=CLIENT_SECRET)
        result = app.acquire_token_for_client(scopes=SCOPES)
        ACCESS_TOKEN = result['access_token']
        print('\u2713')
        return True
    except:
        print('-- Error: authentication failed')
        return False

# Function to save the delta link to a file
def save_delta_link(delta_link):
    try:
        with open(os.path.join(SN_LOCAL_FOLDER, SN_LOCAL_DELTA_FILENAME), 'w') as file:
            file.write(delta_link)
    except:
        print('-- Error: Failed saving delta link')

# Function to read the delta link from a file
def read_delta_link(default):
    try:
        with open(os.path.join(SN_LOCAL_FOLDER, SN_LOCAL_DELTA_FILENAME), 'r') as file:
            return file.read().strip()
    except FileNotFoundError:
        return default

# Detects changes in the remote folder since the last request
def check_subfolder_changes(access_token,user_id, subfolder_id):
    headers = {'Authorization': 'Bearer ' + ACCESS_TOKEN}
    delta_link = read_delta_link(f"https://graph.microsoft.com/v1.0/users/{user_id}/drive/items/{subfolder_id}/delta")
    response = requests.get(delta_link, headers=headers).json()
    if "@odata.deltaLink" in response.keys():
        delta_link_new = response["@odata.deltaLink"]
        save_delta_link(delta_link_new)
        return response["value"]  # Returns changed files
    return None

# Recursive function to parse the remote folder to look for the ID of a named folder
def find_subfolder_id_by_name(access_token, user_id, folder_name, parent_item_id=None):
    headers = {'Authorization': 'Bearer ' + access_token}
    # Start the search from the root or a specified folder
    if parent_item_id:
        url = f'https://graph.microsoft.com/v1.0/users/{user_id}/drive/items/{parent_item_id}/children'
    else:
        url = f'https://graph.microsoft.com/v1.0/users/{user_id}/drive/root/children'

    response = requests.get(url, headers=headers).json()
    if 'value' in response:
        for item in response['value']:
            if item['name'].lower() == folder_name.lower() and item.get('folder'):
                # Folder found
                return item['id']
            elif item.get('folder'):
                # Recurse into subfolder
                found_id = find_subfolder_id_by_name(access_token, user_id, folder_name, item['id'])
                if found_id:
                    return found_id

    return None  # Folder not found

# Creates a temp timestamped sub-folder
def create_workdir(source_subfolder=SN_LOCAL_FOLDER):
    current_time = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
    work_folder = os.path.join(source_subfolder, f'work-{current_time}')
    os.makedirs(work_folder, exist_ok=True)
    return work_folder


# Download a single file asynchronously
async def get_file_obj(drive_id, item_id):
    return await graph_client.drives.by_drive_id(drive_id).items.by_drive_item_id(item_id).content.get()
    
# Download all files asynchronously
async def download_file(work_folder, changed_items):
    for item in changed_items:
        try:
            print(f'>> downloading: {item[2]} ... ', end="")
            file_obj = await get_file_obj(item[0],item[1])
            # Now you can use file_obj as needed
            print('\u2713')
            with open(os.path.join(work_folder, item[2]), 'wb') as file:
                file.write(file_obj) 
        except:
            print('** Failed!')   

# Build the list of titles, based on the JSON Metadata
# Though most of the info is in the '__titles__' key,
# its corresponding value doesn't include the page location
# that is weirdly encoded as separate keys
def built_titles_list(a_json):
    def int_to_str_with_leading_zeros(num):
        return str(num).zfill(4)
    # Initialize dict of Titles
    titles_dict = {}
    result = []
    summary_height = TITLE_VERTICAL_SPACE
    summary_page_nb = 0
    # Retrieve the '__footer__' key
    footer = a_json['__footer__']

    # Step 1: Parse the keys to get the page # and built the dict
    # Warning: This is based on observing the un-commented Supernote Tools output
    for a_key in footer.keys():
        if a_key[0] == 'T':
            try:
                if a_key[:6] == 'TITLE_':
                    a_page= int(a_key[6:10])
                    title_id = a_key[10:18]
                    titles_dict[title_id] = {'TITLEPAGE':a_page }
            except:
                pass
    
    # Step 2: Update the dict with the titles metadata
    titles_metadata_list = footer['__titles__']
    for a_title in titles_metadata_list:
        a_title_rect = a_title['TITLERECT']
        a_title_rect_list = a_title_rect.split(',')
        # Rearrange coordinates. SN has them as (horizontal, vertical, delta horizontal, delta vertical). We want them to be 
        # PIL like, by computing absolute coordinate of second point (opposite diagonal of rect)
        rearranged_list = [int(a_title_rect_list[0]),int(a_title_rect_list[1]), int(a_title_rect_list[0])+int(a_title_rect_list[2]), int(a_title_rect_list[1])+int(a_title_rect_list[3])]
        
        # Increment the remaining vertical space available
        # TODO: Comment the logic of all this or hate yourself in the future
        
        if summary_height > MAX_VERTICAL_PIXELS-int(a_title_rect_list[3]):
            summary_page_nb += 1
            summary_height = TITLE_VERTICAL_SPACE
        a_title['TITLERECT'] = rearranged_list
        a_title['SUMMARYPAGE'] = summary_page_nb
        a_title['SUMMARYHEIGTH'] = summary_height
        a_title['TITLEINDENT'] = TITLE_STYLES[a_title['TITLESTYLE']]
        title_id = int_to_str_with_leading_zeros(a_title_rect_list[1]) + int_to_str_with_leading_zeros(a_title_rect_list[0])
        a_dict = titles_dict[title_id]
        result.append({**a_dict, **a_title})
        summary_height += int(a_title_rect_list[3]) + TITLE_VERTICAL_SPACE
    # Though the SN tool already has the titles in sequence, we do a sort by page, y position, then x position
    return sorted(result, key=lambda x: (x['TITLEPAGE'], x['TITLERECT'][1], x['TITLERECT'][0]))


# Create the picture of the table of contents, update location of titles in the table of contents
def extract_titles_bitmap(file_folder,file_name,max_page_nb):

    # TODO: Make the script more efficient by replacing this repetitve filename split
    basename, extension = os.path.splitext(file_name)
    titles_metadata_folder = os.path.join(work_folder,basename)
    titles_metadata_filename = os.path.join(titles_metadata_folder,basename + '_titles.json')

    # Read the titles json from file
    with open(titles_metadata_filename, 'r') as file:
        titles_metadata = json.loads(file.read().strip())

    # Computes the number of leading zeros
    z_fill_max = len(str(max_page_nb))

    # Create a blank summary picture
    summary_picture = Image.new('RGBA', (MAX_HORIZONTAL_PIXELS, MAX_VERTICAL_PIXELS), (255, 255, 255))
    summary_picture_nb = 0
    summary_filename = os.path.join(titles_metadata_folder,'p_'+ str(summary_picture_nb).zfill(z_fill_max)+'.png')

    # Parsing all titles
    for a_title in titles_metadata:
        a_title_rect = a_title['TITLERECT']
        a_title_indent = a_title['TITLEINDENT']
        page_nb_str = str(a_title['TITLEPAGE']-1).zfill(z_fill_max)
        page_bitmap_name = f'page_{page_nb_str}.png'
        page_bitmap = Image.open(os.path.join(titles_metadata_folder,page_bitmap_name))
        title_rect = (a_title_rect[0],a_title_rect[1],a_title_rect[2],a_title_rect[3])
        title_bitmap = page_bitmap.crop(title_rect).convert('RGBA')
        title_summary_page = a_title['SUMMARYPAGE']
        title_summary_heigth = a_title['SUMMARYHEIGTH']

        # Code to take into account that the TOC may have several pages
        if title_summary_page > summary_picture_nb:
            summary_picture.save(summary_filename) # Saving the current TOC page
            summary_picture_nb += 1
            summary_picture = Image.new('RGBA', (MAX_HORIZONTAL_PIXELS, MAX_VERTICAL_PIXELS), (255, 255, 255))
            summary_filename = os.path.join(titles_metadata_folder,'p_'+ str(summary_picture_nb).zfill(z_fill_max)+'.png')

        # Pasting the tile bitmap on the current TOC page
        x_pasting_location = TITLE_HORIZONTAL_SPACE*a_title_indent
        y_pasting_location = title_summary_heigth
        summary_picture.paste(title_bitmap, (x_pasting_location, y_pasting_location), title_bitmap)

        # TODO: Refactor this ugly coding. For the time being, storing under that key both the position and the TOC page number (last entry of index 4)
        a_title['LOCATION'] = [x_pasting_location, y_pasting_location, x_pasting_location + a_title_rect[2] - a_title_rect[0],  y_pasting_location + a_title_rect[3] - a_title_rect[1], summary_picture_nb ]
    
    # Saving the TOC page
    summary_picture.save(summary_filename)

    # Saving the titles metadata that now contain each title position within the TOC
    with open(titles_metadata_filename, 'w') as file:
        file.write(json.dumps(titles_metadata, indent=4)) 
    # Returns the number of pages of the TOC; this offset is needed to properly link the titles
    return summary_picture_nb + 1


# Extracts all png from a .note file, including its metadata, stored in a JSON using SN Tool code
def extract_png(work_folder, file_name):
    # Retrieving base filename and extension
    basename, extension = os.path.splitext(file_name)
    
    if extension.lower() == '.note':

        file_path = os.path.join(work_folder,file_name)
        # Creates a sub-directory for the given file
        file_sub_folder = os.path.join(work_folder,basename)

        if not os.path.exists(file_sub_folder):
            os.makedirs(file_sub_folder)

        # Reading metadata of the note ans saving it as json
        # TODO: Modify source code because parse_metadata has a lot of similar code
        # running when loading the note (see below)

        with open(file_path, 'rb') as f:
            metadata = sn.parse_metadata(f)

        metadata_json = metadata.to_json(indent=2)
        metadata_json_filename = basename+'.json'
        
        with open(os.path.join(file_sub_folder,metadata_json_filename), 'w') as fjson:
            fjson.write(metadata_json)

        titles_json = json.dumps(built_titles_list(json.loads(metadata_json)), indent=4)
        titles_json_filename = basename+'_titles.json'

        with open(os.path.join(file_sub_folder,titles_json_filename), 'w') as tjson:
            tjson.write(titles_json)

        # Loading the notebook
        notebook = sn.load_notebook(file_path)

        # Getting the number of pages
        total = notebook.get_total_pages()

        # Instantiating the note converter
        converter = ImageConverter(notebook)

        # Saving each page as a png file
        max_digits = len(str(total))
        for i in range(total):
            # append page number between filename and extention
            numbered_filename = os.path.join(file_sub_folder,'page_' + str(i).zfill(max_digits) + '.png')
            img = converter.convert(i, vo)
            img.save(numbered_filename, format='PNG')
        return total


# Convert all pages from a .note file, to a single pdf file, using SN Tool code
# TODO: Something is wrong with that code in terms of resources and time
# Planning to redo with Pymupdf, since we already have all page pictures. The
# reason to use the SN Tool code in the meantime is that I did not implement the 
# non-title links yet (internal, external, urls...)
def extract_pdf(work_folder, file_name):
    # Retrieving base filename and extension
    basename, extension = os.path.splitext(file_name)
    
    if extension.lower() == '.note':

        file_path = os.path.join(work_folder,file_name)
        # Creates a sub-directory for the given file
        file_sub_folder = os.path.join(work_folder,basename)
        pdf_filename = os.path.join(file_sub_folder,basename+'.pdf')

        if not os.path.exists(file_sub_folder):
            os.makedirs(file_sub_folder)

        # Loading the notebook
        notebook = sn.load_notebook(file_path)

        # Instantiating the note converter
        converter = PdfConverter(notebook)

        data = converter.convert(-1, True, enable_link=True) # minus value means converting all pages
        full_pdf_name = os.path.join(file_sub_folder,pdf_filename)
        with open(full_pdf_name, 'wb') as fpdf:
            fpdf.write(data)
        return full_pdf_name

# Not used in this project yet, but the idea is to use it in the future to ocr
# the handwritten notes, use the rect of json to draw boxes on the pdf
# so that the (entire) pdf notebook has an OCR layout on top, leaving the option to
# the user to keep some of the original handwritten sections. 
# Requires creating a resource in MS Azure. So far for me and after thousands of such API calls
# I am still in the free tier
def submit_png_to_cvision(file_path):
    """ Submits a png image to Microsoft Azure Computer Vision
        EndPoint. Details about the endpoint, as well as the API key,
        are stored in the environment variables.
        
        Note that the endpoint contains reference to the resource that
        has been provisioned on Microsoft Azure
        """    
    # Loads environment variables
    DCHECKS_AVS_END_POINT = os.environ.get('DCHECKS_AVS_END_POINT')
    DCHECKS_AVS_KEY = os.environ.get('DCHECKS_AVS_KEY')

    url = f'{DCHECKS_AVS_END_POINT}/computervision/imageanalysis:analyze?api-version=2023-10-01&features=read&model-version=latest&language=en'  

    # Get the image content-type
    content_type, _ = mimetypes.guess_type(file_path)
    if content_type is None:
        content_type = 'application/octet-stream'

    # Format the header
    headers = {
        'Content-Type': content_type,
        'Ocp-Apim-Subscription-Key': DCHECKS_AVS_KEY}

    try:
        # Open the image file in binary mode
        files = open(file_path, 'rb')

        # Make a POST request
        print(f'>>>> Contacting Computer Vision @ Microsoft for image: {file_path}')
        response = requests.post(url=url, headers=headers, data=files)

        # Check the response. If valid, return the list of blocks
        if response.ok:
            print(f'>>> Image analysis received!')
            aresp = json.loads(response.content.decode('utf-8'))
            return aresp["readResult"]["blocks"]
            # print(f"File uploaded successfully.: {response.content.decode('utf-8')}")
        else:
            return []
            # print(f"Failed to upload file. Status code: {response.content.decode('utf-8')}")

        # Handle any errors that may occur during the file upload
    except requests.exceptions.RequestException  as e:
        print(f"An error occurred: {e}")
        return []

# Create a new PDF to hold the table of contents
def create_toc_pdf(work_folder, file_name, nb_pages_toc, max_page_nb):
    z_fill_max = len(str(max_page_nb))
    
    doc = fitz.open()
    basename, extension = os.path.splitext(file_name)
    titles_metadata_folder = os.path.join(work_folder,basename)
    toc_pdf_filename = os.path.join(titles_metadata_folder,'toc.pdf')
    png_files = [os.path.join(titles_metadata_folder,'p_'+ str(toc_index).zfill(z_fill_max) + '.png') for toc_index  in range(0,nb_pages_toc)]

    for png in png_files:

        # TODO: Investigate this ratio. Had to look into SN produced pdf and
        # noticed that using 600! DPI with resolutions of 4961x7016
        # 72 is the default DPI by PyMuPdf
        # WARNING: This transformation needs to be factored in when linking 
        rect = fitz.Rect(0, 0, 4961*72/600, 7016*72/600)
        page = doc.new_page(width=rect.width, height=rect.height)
        page.insert_image(rect, filename=png)

    # Save and reduce size
    doc.save(toc_pdf_filename,garbage=4, deflate=True, clean=True)
    doc.close()
    return toc_pdf_filename

# Merge TOC pdf with main notebook pdf
def merge_pdfs(table_of_contents_pdf, main_pdf):
    basename, extension = os.path.splitext(main_pdf)
    output_pdf = basename + '_n.pdf'
    doc = fitz.open(main_pdf)
    toc_doc = fitz.open(table_of_contents_pdf)
    doc.insert_pdf(toc_doc, start_at=0)  # Insert the TOC at the beginning
    doc.save(output_pdf)
    doc.close()
    return output_pdf

# Add the links on every title to the exact location in the Notebook
# Warning: Need to detransform the changes introduced when creating the TOC pdf
# TODO: Investigate the transformation need, make bettr comments or hate yourself in the future
def add_hyperlinks_to_pdf(file_name, nb_pages_toc):
    x_ratio = 4961*72/600/1404
    y_ratio = 7016*72/600/1872
    basename, extension = os.path.splitext(file_name)
    titles_metadata_folder = os.path.join(work_folder,basename)
    titles_metadata_filename = os.path.join(titles_metadata_folder,basename[:-1] + 'titles.json')
    final_pdf = os.path.join(titles_metadata_folder,basename[:-1] + 'f.pdf')

    with open(titles_metadata_filename, 'r') as file:
        titles_metadata = json.loads(file.read().strip())
    
    doc = fitz.open(file_name)
    for a_title in titles_metadata:
        page_nb_toc_title = a_title['LOCATION'][4]
        page_nb_title = a_title['TITLEPAGE']

        # Link from TOC title to title
        page_toc_title = doc[page_nb_toc_title]
        rect = fitz.Rect(x_ratio*a_title['LOCATION'][0], y_ratio*a_title['LOCATION'][1], x_ratio*a_title['LOCATION'][2], y_ratio*a_title['LOCATION'][3])  # Rectangle where the link should be

        a_dict = {
            "kind": fitz.LINK_GOTO,
            "from": rect,
            "page": page_nb_title + nb_pages_toc -1,
            "to": fitz.Point(x_ratio*a_title['TITLERECT'][0],y_ratio*a_title['TITLERECT'][1]),
            "zoom": 0}

        page_toc_title.insert_link(a_dict)
        

        # 'Return link' from title to TOC title
        page_title = doc[page_nb_title+ nb_pages_toc-1]
        rect = fitz.Rect(x_ratio*a_title['TITLERECT'][0], y_ratio*a_title['TITLERECT'][1], x_ratio*a_title['TITLERECT'][2], y_ratio*a_title['TITLERECT'][3])  # Rectangle where the link should be

        a_dict2 = {
            "kind": fitz.LINK_GOTO,
            "from": rect,
            "page": page_nb_toc_title,
            "to": fitz.Point(x_ratio*a_title['LOCATION'][0],y_ratio*a_title['LOCATION'][1]),
            "zoom": 0}

        page_title.insert_link(a_dict2)    

    doc.save(final_pdf)  # Save with modifications
    doc.close()
    return final_pdf


# ---------------- SCRIPT BEGINS HERE -------------------------------------

# STEP 0: Creates a temp folder and pdf folder, if not existing
print('')
if not os.path.exists(SN_LOCAL_FOLDER):
        print(f'>> Creating directory: {SN_LOCAL_FOLDER}... ', end='')
        os.makedirs(SN_LOCAL_FOLDER)
        print('\u2713', end='')

if not os.path.exists(TOC_PDF_FOLDER):
        print(f'\n>> Creating directory: {TOC_PDF_FOLDER}... ', end='')
        os.makedirs(TOC_PDF_FOLDER) 
        print('\u2713', end='')       

# STEP 1: Connecting to Microsoft and setting the Access token
if set_access_token():
    pass
else:
    print('\n>> Exiting app')
    exit(1)

# STEP 2: Find ID of the SuperNote folder to watch
# Since this is a recursive and long request,
# it will be run only if the storage file foor that ID is not found
# First try to see if the ID of the SuperNote folder to watch is already available
try:
    with open(os.path.join(SN_LOCAL_FOLDER, SN_LOCAL_WATCHING_FOLDER_FN), 'r') as file:
        supernote_folder_id = file.read().strip()
except:  
    # If the retrieval of that info failed, launch the reccursive request
    print(f'>> Retrieving ID for folder: {SN_LOCAL_WATCHING_FOLDER} - Please wait ...', end='')
    supernote_folder_id = find_subfolder_id_by_name(ACCESS_TOKEN, USER_ID, SN_LOCAL_WATCHING_FOLDER)
    print('\u2713', end='')
    # Save the ID of the SuperNote folder for future requests
    with open(os.path.join(SN_LOCAL_FOLDER, SN_LOCAL_WATCHING_FOLDER_FN), 'w') as file:
        file.write(supernote_folder_id)

# STEP 3: Query Microsoft Graph to find changes in the SuperNote folder to watch
print(f'\n>> Checking remote changes ... ', end='')
changed_sn = check_subfolder_changes(ACCESS_TOKEN, USER_ID, supernote_folder_id)
if changed_sn == []:
    print('No changes', end='')
    print('\n>> Polling finished without any processing')
else:  
    changed_sn_list = [item for item in changed_sn] # TODO: Not sure this is necessary. it's silly!
    
    if changed_sn_list != []:
        # We have changes, let's handle them. TODO: In the future, look at options based on type of modifications
        changed_items = [(item['parentReference']['driveId'],item['id'], item['name']) for item in changed_sn_list if item['id'] != supernote_folder_id ]
        print(f'found {len(changed_items)} change(s)')
        

        # Creates a timestamped folder
        work_folder = create_workdir()

        # Download all files, asynchronously
        asyncio.run( download_file(work_folder, changed_items))

        # Parse each modified file since the last polling. 
        # TODO: Sort out here the non '.note' files
        for a_file in changed_items:
            # Get the filename
            a_file_name = a_file[2]
            print('\n')
            print('---------------------------------------------------------------')
            print(f'   {a_file_name}')
            print('---------------------------------------------------------------')
            # Extract all pages as PNG files and JSON metadata
            print('>> Extracting notebook pages as pngs ... ', end='')
            max_page_nb = extract_png(work_folder, a_file_name)
            print('\u2713', end='')
            print('\n>> Extracting titles png ... ', end='')
            nb_pages_toc = extract_titles_bitmap(work_folder, a_file_name ,max_page_nb )
            print('\u2713', end='')
            print('\n>> Building Table Of Content (TOC) pdf ... ', end='')
            toc_pdf_filename = create_toc_pdf(work_folder, a_file_name, nb_pages_toc, max_page_nb)
            print('\u2713', end='')
            # Convert the note tp a PDF file
            print('\n>> Converting notebook to PDF ...', end='')
            full_pdf_name = extract_pdf(work_folder, a_file_name)
            print('\u2713', end='')
            # merge toc with main pdf
            print('\n>> Merging TOC with PDF ... ', end='')
            output_pdf = merge_pdfs(toc_pdf_filename, full_pdf_name)
            print('\u2713', end='')
            print('\n>> Adding links to pages ... ', end='')
            final_pdf = add_hyperlinks_to_pdf(output_pdf, nb_pages_toc)
            print('\u2713',end='')
            print('\n>> Moving file ... ', end='')
            # Ensure the destination folder exists
            if not os.path.exists(TOC_PDF_FOLDER):
                os.makedirs(TOC_PDF_FOLDER)
            # Construct the destination file path
            destination_file = os.path.join(TOC_PDF_FOLDER, os.path.basename(final_pdf))
            # Copy the file to the destination folder, overwriting any existing file with the same name
            shutil.move(final_pdf, destination_file)
            print('\u2713',end='')
            print('\n>> Cleaning up temp files ... ', end='')
             # Get the folder of the source file
            shutil.rmtree(os.path.dirname(final_pdf))
            print('\u2713',end='')
            
        shutil.rmtree(work_folder)
        print('\n>> Synchronization & TOC generation finished!')








   

