
# NOTICE: Alteration of [Github supernote-tool parser.py](https://github.com/jya-dev/supernote-tool/blob/master/supernotelib/parser.py)

## parser.py

### <u>Original</u>

```python
def _parse_link_block(self, fobj, address):
    return self._parse_metadata_block(fobj, address)
```

### <u>Modified</u>
> MMB we retrieve the url and add it to the dictionary, if applicable. Not yet used.
```python
import base64

def _parse_link_block(self, fobj, address):
    address_dict = self._parse_metadata_block(fobj, address)
    web_like_link = address_dict['LINKTYPE'] == "4"
    if web_like_link:
        encoded_url = address_dict['LINKFILE']
        url = base64.b64decode(encoded_url).decode()
        address_dict['LINKURL'] = url
    return address_dict
```