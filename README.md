## Setting Up the Project Environment

To run this project, you will need to have Python installed on your system. This project depends on several Python packages, which are specified in the `requirements.txt` file to ensure compatibility.

### Step 1: Clone the Project

First, create a folder and clone this repository to your local machine using GitLab's cloning options. 

### Step 2: Download version 3.12 of Python and create a Virtual Environment (Recommended)

Head to [Python official website](https://www.python.org/downloads/) and download version 3.12.
It's a good practice to use a virtual environment for Python projects to keep dependencies separate and organized. Navigate to the project directory in your terminal and run the following command to create a virtual environment named `env`:

```bash
python -m venv env 
```

### Step 3: Activate the virtual environment

Please use the appropriate command below for your operating system.
You should then see (env) at the beginning of your terminal prompt, indicating that the virtual environment is active.

#### Windows:
```cmd
env\Scripts\activate
```

#### macOS and Linux:
```bash
source env/bin/activate
```

### Step 4: Install Required Packages
With the virtual environment activated, install the project's dependencies by running:
```bash
pip install -r requirements.txt
```
This command installs all the packages listed in requirements.txt with their specified versions.
At one point we will fork the supernotelib code. In the short time, we intend to make a small adjustment to the source code (see NOTICE)

### Step 5: Other Pre-requisites
This  Python script processes Supernote notebooks ('.note' extension) stored on Microsoft OneDrive.

- Supernote: Settings -> Sync -> OneDrive (then configure account and folders to synchronize, include one storing your notes)
- Microsoft Azure: The script needs Microsoft Azure app registration and credentials (See MS Azure section)
- Credentials are stored as environment variables (See Environment Variables section)
- If you want to fully automate, you'll have to create a task in Windows Task Scheduler (See MS Windows Task Scheduler)


### Step 6: Launching
With the dependencies installed and  pre-requisites taken care of, you can now run the project following the specific instructions provided elsewhere in this README.
```bash
python snote.py
```
The program creates a temp folder (by default 'temp'), holding time stamped subfolders, that could be useful for debugging. If the program executes without error, the time stamped folder is deleted once the pdf with table of contents have been created.

It also creates a subfolder to hold the created pdfs (by default 'PDF Notes')
<div style="background-color: #333; border-left: 3px solid red; padding: 10px;">
  <strong>Warning:</strong> The program will <b>always</b> overwrite files in the output folder
</div>

## Other information

### Libraries
See requirements.txt

### Microsoft Azure settings
I have not reviewed yet these [GPT-4 generated settings instructions](https://chat.openai.com/share/b9b3e90a-f686-4d2e-9f2e-4e674b32c512), but glancing at it, they appear correct.

### Variables

- Environment Variables
    - "SN_CLIENT_ID": MS Azure Client ID, obtained when registering the app.
    - "SN_USER_ID":  ObjectID of the user owning the OneDrive folder
    - "SN_TENANT_ID": MS Azure Tenant ID (Your 'account identifier' )
    - "SN_CLIENT_SK": MS Azure secret key for this app 

- Global Variables (See top of snote.py and comments)

